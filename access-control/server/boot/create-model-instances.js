var debug = require('debug')('boot:create-model-instances');
module.exports = function(app){
	var User = app.models.user;
	var Role = app.models.Role;
	var RoleMapping = app.models.RoleMapping;
	var Team = app.models.Team;

	User.create([
    {username: 'John', email: 'john@doe.com', password: 'opensesame'},
    {username: 'Jane', email: 'jane@doe.com', password: 'opensesame'},
    {username: 'Bob', email: 'bob@projects.com', password: 'opensesame'}
  ], function(err, users){
  	if(err) return debug('%j', err);
  	debug(users);

  	users[0].projects.create({
  		name: 'project1',
  		balance: 100
  	}, function(err, project){
  		if(err) return debug(err);
  		debug(project);
  		//add team members

  		Team.create([
  			{ownerId: project.ownerId, memberId: users[0].id},
  			{owberId: project.owberId, memberId: users[1].id}
  		], function(err, team){
  			if(err) return debug(err);
  			debug(team);
  		});
  	});

  	users[1].projects.create({
  		name: 'project2',
  		balance: 100
  	},function(err, project){
  		if(err)
  			return debug(err);
  		debug(project);
  		//add team members
  		Team.create({
  			ownerId: project.ownerId,
  			memberId: users[1].id
  		}, function(err, team){
  			if(err)
  				return debug(err);
  			debug(team);
  		});
  	});

  	//Create the admin role
  	Role.create({
  		name: 'admin'
  	}, function(err, role){
  		if(err)
  			return debug(err);
  		debug(role);
  		//make bob admin
  		role.principals.create({
  			principalType: RoleMapping.USER,
  			principalId: users[2].id
  		}, function(err, principal){
  			if(err)
  				return debug(err);
  			debug(principal);
  		});
  	});
  });
};