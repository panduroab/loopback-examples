var orders = [
{description:"Order 1", total: 5},
{description:"Order 2", total: 3},
{description:"Order 3", total: 15},
{description:"Order 4", total: 2},
{description:"Order 5", total: 1},
{description:"Order 6", total: 9}
];

module.exports = function(server){
	var dataSource = server.datasources.db;
	dataSource.automigrate('Order', function(er){
		if(er)
			throw er;
		var Model = server.models.Order;
		var count = orders.length;
		orders.forEach(function(order){
			Model.create(order, function(er, result){
				if(er)
					return;
				console.log('Order created: ', result);
				count--;
				if(count === 0){
					dataSource.disconnect();
				}
			});
		});
	});
};