var customers = [
	{name: 'Customer A', age: 21},
	{name: 'Customer B', age: 22},
  	{name: 'Customer C', age: 23},
  	{name: 'Customer D', age: 24},
  	{name: 'Customer E', age: 25}
  	];
module.exports = function(server){
    var dataSource = server.datasources.db;
    dataSource.automigrate('Customer', function(er){
    	if(er)
    		throw er
    	var Model = server.models.Customer;
    	var count = customers.length;
    	customers.forEach(function(customer){
    		Model.create(customer, function(er, result){
    			if(er)
    				return;
    			console.log('Customer created: ', result);
    			count--;
    			if(count === 0){
    				dataSource.disconnect();
    			}
    		});
    	});
    	Model.scope('youngFolks', {where: {age: {lte: 22 }}});
    });
};