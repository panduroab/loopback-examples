/*var DataSource = require('loopback-datasource-juggler').DataSource;
var reviews = [
	{product: "Product 1", star: 5},
	{product: "Product 2", star: 4},
	{product: "Product 3", star: 3},
	{product: "Product 4", star: 5},
	{product: "Product 5", star: 1},
];
module.exports = function(server){
	var dataSource = new DataSource('mysql', {
		host: "localhost",
	    port: 3306,
	    database: "loopback_test",
	    username: "root",
	    password: "admin"
     });
	dataSource.on('connected', function(){
		dataSource.automigrate('Review', function(){
			var Model = server.models.Review;
			var count = reviews.length;
			reviews.forEach(function(review){
				Model.create(review, function(er, result){
					if(er)
						return;
					console.log('Review created: ', result);
					count--;
					if(count === 0){
						dataSource.disconnect();
					}
				});
			});
		});
	});
};*/
var reviews = [
	{product: "Product 1", star: 5},
	{product: "Product 2", star: 4},
	{product: "Product 3", star: 3},
	{product: "Product 4", star: 5},
	{product: "Product 5", star: 1},
];
module.exports = function(server){
	var dataSource = server.datasources.db;
	dataSource.automigrate('Review', function(er){
		if(er)
			throw er;
		var Model = server.models.Review;
		var count = reviews.length;
		reviews.forEach(function(review){
			Model.create(review, function(er, result){
				if(er)
					return;
				console.log('Review created: ', result);
				count--;
				if(count === 0){
    				dataSource.disconnect();
				}
			});
		});
	});
};